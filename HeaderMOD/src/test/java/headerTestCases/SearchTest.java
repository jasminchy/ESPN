package headerTestCases;

import base.CommonAPI;
import headerPageObject.Search;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class SearchTest extends CommonAPI
{
    @Test()
    public void searchTest()
    {
        SoftAssert softAssert = new SoftAssert();
        Search sr = PageFactory.initElements(driver,Search.class);
        softAssert.assertTrue(sr.title(),"Amazon");
        sr.search();
        softAssert.assertEquals("Amazon.com: Eloquence JavaScript","Amazon.com: Eloquence JavaScript");
        System.out.println("PASS");
        softAssert.assertAll();
    }



    @Test(enabled = false)
    public void newTest()
    {

    }
}
