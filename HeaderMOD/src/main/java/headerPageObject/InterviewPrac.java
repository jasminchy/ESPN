package headerPageObject;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class InterviewPrac
{
    WebDriver driver = null;

    public void takeSnapShot() throws Exception
    {
        TakesScreenshot screenshot = ((TakesScreenshot)driver);
        File srcFile =  screenshot.getScreenshotAs(OutputType.FILE);

        File destFile = new File("/Users/imrankabir/IdeaProjects/ESPN/HeaderMOD/src/scrinShot");

        FileUtils.copyFile(srcFile,destFile);
    }

    @BeforeMethod
    public void setUp()
    {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.home")+"/IdeaProjects/ESPN/Generic/drivers/chromedriver");
        driver = new ChromeDriver();
        driver.get("https://www.facebook.com/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);

        //WebDriverWait wait = new WebDriverWait(driver, 20);
        //wait.until(ExpectedConditions.elementToBeClickable(By.xpath("")));
        //wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("")));
    }

    @AfterMethod
    public void teirDown()
    {
        driver.close();
    }

    //Iterate radio button
    @Test
    public void testA() throws Exception
    {
        List<WebElement> radioBtns =  driver.findElements(By.xpath("//input[@type = \"radio\"]"));

        String radioValue = "";

        try {
            for(int i = 0; i < radioBtns.size(); i++){
                String e = radioBtns.get(i).getAttribute("value");

                if(e.equalsIgnoreCase(radioValue))
                {
                    radioBtns.get(i).click();
                }else if (e.equalsIgnoreCase(radioValue)){
                    radioBtns.get(i).click();
                }
            }
        }catch (Exception e){
            takeSnapShot();
        }
    }

    @Test
    public void testB()
    {
        WebDriverWait wait = new WebDriverWait(driver,20);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"u_0_9\"]"))).click();



    }


}
