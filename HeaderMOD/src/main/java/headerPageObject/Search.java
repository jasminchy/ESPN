package headerPageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Search
{

    WebDriver driver;
    @FindBy(css = "#twotabsearchtextbox")
    WebElement searchBox;
    @FindBy(css = "#nav-search > form > div.nav-right > div > input")
    WebElement btn;
    @FindBy(xpath = "//*[@id=\"nav-logo\"]/a[1]/span[1]")
    WebElement title;

    //get the locator
    public boolean title()
    {
        return title.isDisplayed();
    }

    //Send keys and click
    public void search()
    {
        searchBox.sendKeys("Eloquence JavaScript");
        btn.click();
    }

    //Explicate wait
    public void explWait()
    {
        WebDriverWait wait = new WebDriverWait(driver,20);
        wait.until(ExpectedConditions.elementToBeClickable(By.id("")));
        // alertPresent()
        wait.until(ExpectedConditions.alertIsPresent());
        // elementSelectionStateToBe()
        wait.until(ExpectedConditions.elementSelectionStateToBe(title,title.isSelected()));
        // elementToBeClickable
        wait.until(ExpectedConditions.elementToBeClickable(btn));
        // elementToBeSelectable()
        wait.until(ExpectedConditions.elementToBeSelected(By.id("")));
        // frameToBeAvailableAndSwitchToIt()
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("locator"));
        // invisibilityOfElementLocated()
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("")));
        // invisibilityOfElementWithText()
        wait.until(ExpectedConditions.invisibilityOfElementWithText(By.id(""),""));
        // presenceOfAllElementLocatedBy()
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("")));
        // textToBePresentInElement()
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.id(""), ""));
        // titleIs()
        wait.until(ExpectedConditions.titleIs(""));
        //titleContains()
        wait.until(ExpectedConditions.titleContains(""));
        //visibilityOf()

    }
}
