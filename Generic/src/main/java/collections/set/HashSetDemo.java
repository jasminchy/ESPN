package collections.set;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class HashSetDemo
{
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        set.add("Imran");
        set.add("Nafee");
        set.add("Jasmin");
        set.add("");


        TreeSet<Integer> set1 = new TreeSet(set);

        System.out.println(set1.first());
    }
}
