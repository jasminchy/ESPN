package listener;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class ListenerUtil implements ITestListener
{

    @Override
    public void onTestStart(ITestResult result) {
        System.out.println("TestCase started and details are "+result.getName());
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        System.out.println("TestCase sucess and details are "+result.getName());

    }

    @Override
    public void onTestFailure(ITestResult result) {
        System.out.println("TestCase failed and details are "+result.getName());

    }

    @Override
    public void onTestSkipped(ITestResult result ) {
        System.out.println("TestCase skiped and details are "+result.getName());

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ITestContext iTestContext) {

    }

    @Override
    public void onFinish(ITestContext iTestContext) {

    }
}
